describe('App', function() {
    it('should have the right title', () => {
        browser.url('/');
        const title = browser.getTitle();
        expect(title).toBe('Shopping Cart app');
    });

    it('should update cart badge', () => {
        browser.url('/');
        browser.waitForExist('button[data-test-id="product-1"]', 4000);
        browser.leftClick('button[data-test-id="product-1"]');
        const itemsInCart = browser.getText('[data-test-id="itemsInCart"]');
        expect(itemsInCart).toBe('(1)');
    });

    it('should add product to cart', () => {
        browser.url('/');
        browser.waitForExist('button[data-test-id="product-2"]', 4000);
        browser.leftClick('button[data-test-id="product-2"]');
        browser.leftClick('a[href="/cart"]');
        const hasProducts = browser.isExisting('[data-test-id="cartItem-2"]')
        expect(hasProducts).toBe(true);
    });
});