import React from 'react';
import Cart from '../../components/Cart/Cart';

function ShoppingCartPage() {
    return (
        <div>
            <h1>Your Cart </h1>
            <Cart />
        </div>
    );
}

export default ShoppingCartPage;
