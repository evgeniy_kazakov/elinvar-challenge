import React from 'react';
import ProductList from '../../components/ProductList/ProductList';

function ProductsPage() {
    return (
        <div>
            <h1>Catalog</h1>
            <ProductList />
        </div>
    );
}

export default ProductsPage;

