import { combineReducers } from 'redux';
import productsReducer from '../components/ProductList/reducer';
import cartReducer from '../components/Cart/reducer';

const reducer = combineReducers({
    products: productsReducer,
    cart: cartReducer,
});

export default function rootReducer(state, action) {
    return reducer(state, action);
}
