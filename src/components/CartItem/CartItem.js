import React from 'react';
import PropTypes from 'prop-types';
import styled from 'react-emotion';

const CartItemContent = styled('div')`
  display: flex;
  padding: 20px 0;
  align-items: center;
  justify-content: space-around;
`;

const ItemImage = styled('img')`
  width: 160px;
  height: 80px;
  background-color: #999;
`;

const ItemName = styled('div')`
  font-size: 18px;
  font-weight: bold;
`;

function CartItem (props) {
    const {
        productID,
        name,
        unitPrice,
        unitsInStock,
        image,
        unitsInCart,
        onChangeUnitsCount,
        onRemoveItemFromCart,
    } = props;

    return (
        <CartItemContent data-test-id={`cartItem-${productID}`}>
            <ItemImage src={image} />
            <div>
                <ItemName>{name}</ItemName>
                <div>Items in stock: {unitsInStock}</div>
            </div>
            <div>Price: {unitPrice}$</div>
            <div>
                <input
                    type="number"
                    min="1"
                    value={unitsInCart}
                    onChange={({ target: { value } }) => {
                        onChangeUnitsCount(productID, parseInt(value, 10));
                    }}
                />
            </div>
            <div>
                <button
                    type="button"
                    onClick={() => onRemoveItemFromCart(productID)}
                >
                    Remove
                </button>
            </div>
        </CartItemContent>
    );
}

CartItem.propTypes = {
    productID: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    unitPrice: PropTypes.number.isRequired,
    unitsInStock: PropTypes.number.isRequired,
    unitsInCart: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
    onRemoveItemFromCart: PropTypes.func.isRequired,
    onChangeUnitsCount: PropTypes.func.isRequired,
};

export default CartItem;
