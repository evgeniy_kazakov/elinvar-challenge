import React from 'react';
import PropTypes from 'prop-types';
import styled from 'react-emotion';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { size } from 'lodash';
import { filterEmptyFields } from '../../utils';

const NavigationContent = styled('nav')`
  display: flex;
  padding: 20px;
  align-items: center;
  justify-content: space-between;
`;

function Navigation(props) {
    const { itemsInCart } = props;
    return (
        <NavigationContent>
            <Link to="/">Home</Link>
            <Link to="/top5">Top-5 Products</Link>
            <Link to="/cart">
                Your Cart
                <span data-test-id="itemsInCart">
                    {itemsInCart > 0 ? `(${itemsInCart})` : ''}
                </span>
            </Link>
        </NavigationContent>
    );
}

Navigation.propTypes = {
    itemsInCart: PropTypes.number.isRequired,
};

function stateToProps(state) {
    return { itemsInCart: size(filterEmptyFields(state.cart)) };
}
export default connect(stateToProps)(Navigation);
