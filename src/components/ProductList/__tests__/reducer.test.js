import reducer from '../reducer';
import types from '../actionTypes';

const initialState = Object.freeze({
    isLoading: false,
    items: {},
    error: null,
    isItemsLoaded: false,
});

describe('Products reducer', () => {
    it('initialize request indicator', () => {
        const action = { type: types.GET_PRODUCTS };
        const nextState = reducer(initialState, action);
        expect(nextState).toEqual({ ...initialState, isLoading: true });
    });

    it('add products on request success', () => {
        const products = [
            {
                "productID": 4,
                "name": "Chef Anton's Cajun Seasoning",
                "description": "",
                "unitPrice": 22,
                "unitsInStock": 53,
                "image": "http://lorempixel.com/400/200/technics/"
            },
            {
                "productID": 5,
                "name": "Chef Anton's Gumbo Mix",
                "description": "",
                "unitPrice": 21.35,
                "unitsInStock": 0,
                "image": "http://lorempixel.com/400/200/technics/"
            },
        ];
        const action = {
            type: types.GET_PRODUCTS_SUCCESS,
            payload: {
                data: { products }
            }
        };
        const state = { ...initialState, isLoading: true, };
        const nextState = reducer(state, action);

        expect(nextState).toEqual({
            ...state,
            isLoading: false,
            isItemsLoaded: true,
            items: {
                4: products[0],
                5: products[1]
            }
        });
    });

    it('add error on request fail', () => {
        const action = {
            type: types.GET_PRODUCTS_FAIL,
            error: {
                message: 'error!'
            }
        };
        const state = { ...initialState, isLoading: true, };
        const nextState = reducer(state, action);

        expect(nextState).toEqual({
            ...state,
            isLoading: false,
            error: { message: 'error!' }
        });
    });
});