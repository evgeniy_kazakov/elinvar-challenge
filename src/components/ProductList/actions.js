import types from './actionTypes';
import axios from 'axios';

export const getProducts = () => {
    return (dispatch, getState) => {
        const state = getState();
        if (state.products.isItemsLoaded) {
            return;
        }

        dispatch({ type: types.GET_PRODUCTS });
        return axios.get('https://private-3efa8-products123.apiary-mock.com/products')
            .then((response) => {
                dispatch({
                    type: types.GET_PRODUCTS_SUCCESS,
                    payload: {
                        data: response.data
                    }
                })
            })
            .catch(() => {
                dispatch({
                    type: types.GET_PRODUCTS_FAIL,
                    error: {
                        message: 'Failed to load products'
                    }
                })
            })
        ;
    }
};


