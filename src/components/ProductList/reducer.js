import { reduce } from 'lodash';
import { createReducer } from '../../utils';
import types from './actionTypes';

const initialState = {
    isLoading: false,
    items: {},
    error: null,
    isItemsLoaded: false,
};

export default createReducer({
    [types.GET_PRODUCTS]: () => ({
        isLoading: true,
    }),
    [types.GET_PRODUCTS_SUCCESS]: (state, payload) => {
        const items = reduce(payload.data.products, (result, product) => {
            result[product.productID] = product;
            return result;
        }, {});
        return {
            items,
            isLoading: false,
            isItemsLoaded: true,
        };
    },
    [types.GET_PRODUCTS_FAIL]: (status, payload, error) => {
        return {
            isLoading: false,
            error,
        };
    },
}, initialState);