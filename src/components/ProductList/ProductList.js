import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'react-emotion';
import { connect } from 'react-redux';
import { map } from 'lodash';
import Product from '../Product/Product';
import { addProductToCard } from '../Cart/actions';
import { getProducts } from './actions';

const ProductListContent = styled('div')`
  display: flex;
  flex-wrap: wrap;
`;

class ProductList extends Component {
    componentDidMount() {
        this.props.getProducts();
    }

    render() {
        const {
            isLoading,
            items,
            addProductToCard,
            error,
        } = this.props;

        if (isLoading) {
            return (
                <h3>Loading...</h3>
            );
        }

        if (error) {
            return <h3>{error.message}</h3>
        }

        return (
            <ProductListContent>
                {map(items, (product) => {
                    return (<Product
                        key={product.productID}
                        {...product}
                        onAddToCartClick={addProductToCard}
                    />)
                })}
            </ProductListContent>
        );
    }
}

ProductList.propTypes = {
    items: PropTypes.object,
    isLoading: PropTypes.bool.isRequired,
    error: PropTypes.object,
    getProducts: PropTypes.func.isRequired,
};

function stateToProps(state) {
    return state.products;
}

export default connect(stateToProps, { addProductToCard, getProducts })(ProductList);
