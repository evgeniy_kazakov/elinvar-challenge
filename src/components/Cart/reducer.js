import { createReducer } from '../../utils';
import {
    defaultTo,
} from 'lodash';
import types from './actionTypes';
const initialState = { };

export default createReducer({
    [types.ADD_PRODUCT_TO_CART]: (state, payload) => {
        const { productID } = payload;
        return { [productID]: defaultTo(state[productID], 0) + 1 };
    },
    [types.REMOVE_PRODUCT_FROM_CART]: (state, payload) => {
        return { [payload.productID]: null };
    },
    [types.CHANGE_PRODUCT_UNITS_IN_CART]: (state, payload) => {
        return { [payload.productID]: payload.value };
    }
}, initialState);