import React from 'react';
import PropTypes from 'prop-types';
import styled from 'react-emotion';
import { connect } from 'react-redux';
import {
    map,
    pick,
    isEmpty,
    reduce,
} from 'lodash';
import CartItem from '../CartItem/CartItem';
import {
    removeProductFromCard,
    changeProductUnitsInCart
} from './actions';
import { filterEmptyFields } from '../../utils';

const CartContent = styled('div')`
  width: 800px;
`;

function Cart (props) {
    const {
        items,
        totalPrice,
        removeProductFromCard,
        changeProductUnitsInCart,
    } = props;

    if (isEmpty(items)) {
        return (
            <div>
                <h3>Cart is empty</h3>
            </div>
        );
    }

    return (
        <CartContent>
            {map(items, (item) => (
                <CartItem
                    key={item.productID}
                    {...item}
                    onRemoveItemFromCart={removeProductFromCard}
                    onChangeUnitsCount={changeProductUnitsInCart}
                />
            ))}
            <div>
                <h3>Total Price: {totalPrice}$</h3>
            </div>
        </CartContent>
    );
}

Cart.propTypes = {
    items: PropTypes.array.isRequired,
    totalPrice: PropTypes.number.isRequired,
    removeProductFromCard: PropTypes.func.isRequired,
    changeProductUnitsInCart: PropTypes.func.isRequired,
};

function stateToProps(state) {
    const { products, cart } = state;
    const cartKeys = Object.keys(filterEmptyFields(cart));
    const items = map(
        pick(products.items, cartKeys),
        (item) => ({ ...item, unitsInCart: cart[item.productID] })
    );
    const totalPrice = reduce(items, (sum, { unitsInCart, unitPrice }) => sum + (unitsInCart * unitPrice), 0);

    return { items, totalPrice };
}

const actions = {
    removeProductFromCard,
    changeProductUnitsInCart,
};

export default connect(stateToProps, actions)(Cart);
