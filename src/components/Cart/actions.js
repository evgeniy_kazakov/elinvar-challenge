import types from './actionTypes';

export const addProductToCard = (productID) => ({
    type: types.ADD_PRODUCT_TO_CART,
    payload: { productID },
});

export const removeProductFromCard = (productID) => ({
    type: types.REMOVE_PRODUCT_FROM_CART,
    payload: { productID },
});

export const changeProductUnitsInCart = (productID, value) => ({
    type: types.CHANGE_PRODUCT_UNITS_IN_CART,
    payload: {
        productID,
        value,
    },
});


