import React, { Component } from 'react';
import {
    BrowserRouter,
    Route,
    Switch,
} from "react-router-dom";
import { Provider } from 'react-redux';
import ProductsPage from '../../pages/ProducstsPage/ProductsPage';
import ShoppingCartPage from '../../pages/ShoppingCartPage/ShoppingCartPage';
import Top5ProductsPage from '../../pages/Top5ProductsPage/Top5ProductsPage';
import Naviagation from '../Navigation/Navigation';

class App extends Component {
    render() {
        const { store } = this.props;
        return (
            <Provider store={store}>
                <BrowserRouter>
                    <div>
                        <Naviagation />
                        <Switch>
                            <Route path="/" exact component={ProductsPage} />
                            <Route path="/top5" component={Top5ProductsPage} />
                            <Route path="/cart" component={ShoppingCartPage} />
                        </Switch>
                    </div>
                </BrowserRouter>
            </Provider>
        );
    }
}

export default App;
