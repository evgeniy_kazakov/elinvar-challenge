import React from 'react';
import { mount } from 'enzyme';
import { noop } from 'lodash';
import Product from '../Product';

describe('Product', () => {
    const baseProps = Object.freeze({
        productID: 4,
        name: "Chef Anton's Cajun Seasoning",
        description: "",
        unitPrice: 22,
        unitsInStock: 53,
        image: "http://lorempixel.com/400/200/technics/"
    });

    it('renders correctly', () => {
        const props = {
            ...baseProps,
            onAddToCartClick: noop
        };
        const wr = mount(<Product {...props} />);
        expect(wr).toMatchSnapshot();
    });

    it('call onAddToCartClick', () => {
        const props = {
            ...baseProps,
            onAddToCartClick: jest.fn(),
        };
        mount(<Product {...props} />)
            .find(`button[data-test-id="product-4"]`)
            .simulate('click')
        ;

        expect(props.onAddToCartClick).toBeCalledWith(4);
    });
});