import React from 'react';
import PropTypes from 'prop-types';
import styled from 'react-emotion';

const ProductContent = styled('div')`
  width: 440px;
  height: 300px;
  padding: 20px;
`;

const ProductImage = styled('img')`
  width: 400px;
  height: 200px;
  background-color: #999;
`;

const ProductName = styled('div')`
  font-size: 18px;
  font-weight: bold;
`;

const Row = styled('div')`
    display: flex;
    justify-content: space-between;
`;

function Product (props) {
    const {
        name,
        productID,
        description,
        unitPrice,
        unitsInStock,
        image,
        onAddToCartClick,
    } = props;

    return (
        <ProductContent>
            <div>
                <ProductImage src={image} />
            </div>
            <ProductName>{name}</ProductName>
            <div>{description}</div>
            <Row>
                <div>
                    <div>Price: {unitPrice}$</div>
                    <div>In stock: {unitsInStock}</div>
                </div>
                <button
                    type="button"
                    data-test-id={`product-${productID}`}
                    onClick={() => onAddToCartClick(productID)}
                >
                    Add to cart
                </button>
            </Row>
        </ProductContent>
    );
}

Product.propTypes = {
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    unitPrice: PropTypes.number.isRequired,
    unitsInStock: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
};

export default Product;
